package com.tegarriszki_10181075.sharedpreference_tegar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView textView;
    private EditText editText;
    private Button applybutton;

    private String text;

public static final String SHARED_PREFS= "sharedPrefs";
public static final String TEXT = "text";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.txtjudul);
        editText = (EditText) findViewById(R.id.edittext);
        applybutton = (Button) findViewById(R.id.applybutton);

        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView.setText(editText.getText().toString());
            }
        });
        applybutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                applybutton();
            }

        });
        loadData();
        updateViews();
    }
public void applybutton(){
    SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
    SharedPreferences.Editor editor = sharedPreferences.edit();

    editor.putString(TEXT, textView.getText().toString());
    
    editor.apply();

    Toast.makeText(this, "Data tersimpan", Toast.LENGTH_SHORT).show();
}
public void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        text = sharedPreferences.getString(TEXT, " ");

}
public void updateViews() {
        textView.setText(text);
}
}